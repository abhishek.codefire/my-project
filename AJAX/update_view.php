<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h2>Update User</h2>
<img src='assets/images/<?= $model->photo; ?>' height="200" width="200">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'update-form']); ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'photo')->fileInput()->label('Profile Photo'); ?>

<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>
<?php $script = <<< JS
//var form=jquery('update-form');

$('#update-form').on('beforeSubmit',function(e)
{
    e.preventDefault();
    //var \$form=$(this);
    var formData = new FormData($('form')[0]);
    //console.log(formData);
    $.ajax({
        url:$('#update-form').attr('action'),
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        success: function (data) {
            alert(data);
            // if(data==1)
            // {
                //$('#modal').modal('show');
                //$(document).find('#modal').modal('hide');
                $('#modal').modal('hide');
                location.reload();
                //$.pjax.reload({container:'#index'});
            //}

        }
    })
    // $.post(
    //     \$form.attr("action"),
    //     \$form.serialize()
    // )
    // .done(function (result) {
    //     if(result==1)
    //     {
    //         //$(\$form).trigger("reset");
    //         $(document).find('#modal').modal('hide');
    //         $.pjax.reload({container:'#index'});

    //     }            
    //     else
    //     {
    //         //alert("hiii");
    //         $(\$form).trigger("reset");
    //         $("#message").html(result.message);
    //     }
    // }).fail(function()
    // {
    //     console.log("server-error");
    // })
    // return false;
})
JS;
$this->registerJs($script);
?>