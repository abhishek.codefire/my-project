<?php include 'connect.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>CRUD With AJAX</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <style>
      h1
      {
        text-align:center;
        background-color:orange;
        color:black;
      }

    table,th,td{
        text-align:center;
    }
  </style>
</head>
<body>
<div class="container"><br>
  <h1>AJAX CRUD Application Design By Abhishek</h1><br><br><br>
  <div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Search">
      <button class="btn btn-secondary" type="submit">Search</button>
     </div>
   <!-- Button to Open the Modal -->
   <button  style="float:right; margin-bottom:20px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#myModal"><i class="material-icons"></i><span>Add User</span></button>
   <div id="records_contant"></div>
   <div id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Add User</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>First Name :</label>
							<input type="text" id="firstname" name="firstname" class="form-control" required>
						</div>
            <div class="form-group">
							<label>Last Name :</label>
							<input type="text" id="lastname" name="lastname" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Email :</label>
							<input type="email" id="email" name="email" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="pass">Password :</label>
							<input type="password" id="pass" name="pass" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="conpassword">Confirm Password :</label>
							<input type="password" id="conpassword" name="conpassword" class="form-control" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success" id="add_btn" data-dismiss="modal" onclick="addRecord()">Add</button>
            <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close">
					</div>
			</div>
		</div>
    </div>
    <div class="modal fade" id="updateData">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      <form id="myForm" name="form" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <h4 class="modal-title">Edit user</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="edit">
            <table>
            </table>  
        </div>
        </form>
      </div>
    </div>
  </div> 

  <div class="modal fade" id="viewModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      <form id="myForms" name="form" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <h4 class="modal-title">View user</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="view-table">
            <table>
            </table>  
        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-danger"  data-dismiss="modal" value="close">close</button>
        </div>
        </form>
      </div>
    </div>
  </div> 
<div id="target-content"></div>
</div>
<script type="text/javascript" src="script.js"></script>
<script>
$(document).on('click', '#photo-submit', function () 
{
  //var form = $(this).get('#pic-form');
  var formData = new FormData();
  //var fd = new FormData();
  var files = $('#image')[0].files;
  var id = $(this).data("id");
  var fname = $("#fname").val();
  var lname = $("#lname").val();
  // Check file selected or not
  if (files.length > 0)
  {
    formData.append('file', files[0]);
    formData.append('id', id);
    formData.append('fname', fname);
    formData.append('lname', lname);
  }
  else
  {
    formData.append('id', id);
    formData.append('fname', fname);
    formData.append('lname', lname);
  }
  $.ajax({
        url: "updateUser.php",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,//we are sending file
        success: function (data)
         {
            if (data == 1) 
            {
              $("#update-msg").show().delay(2000).fadeOut();
              load_data();
              $("#updateData").hide();
            }
            else 
            {
               $("#update-msg").html("No");
            }
          }

  });
});
</script>
</body>
</html>