$(document).ready(function ()
{
    readRecords();
    $(document).on("click", ".editUser", function (e)
    {
        $("#updateData").show();
        var id = $(this).data("id");
        $.ajax({
        url: "update.php",
        type: "POST",
        data: { id: id },
        success: function (data)
        {
            $("#edit").html(data);
        }
      });
    });

    $(".page-link").click(function(){
        var id = $(this).attr("id");
        $.ajax({
            url: "connect.php",
            type: "POST",
            data: {
                page : id
            },
            cache: false,
            success: function(data){
                $("#target-content").html(data);
            }
        });
    });
    $(document).on("click", ".viewUser", function (e) {
        $("#viewModal").show();
        var id = $(this).data("id");
        $.ajax({
            url: "view.php",
            type: "POST",
            data: { id: id },
            success: function (data) 
            {
                $("#view-table").html(data);
            }
        });
    }); 
});

function readRecords()
{
  var readrecord ="readrecord";
  $.ajax({
          url:"connect.php",
          type:'post',
          data: { readrecord: readrecord },
          success:function(data)
          {
            $('#target-content').html(data);
          }, 
      });
}

function addRecord()
{
  var firstname = $('#firstname').val();
  var lastname = $('#lastname').val();
  var email = $('#email').val();
  var pass = $('#pass').val();
  var conpassword = $('#conpassword').val();
  if(firstname!="" && lastname!="" && email!="" && pass!="" && conpassword!="")
  {
    $.ajax({
          url:"connect.php",
          type:'post',
          data: { 
            firstname :firstname,
            lastname :lastname,
            email :email,
            pass :pass,
            conpassword :conpassword
          },
          success:function(data)
          {
           console.log("New record created successfully !");
           readRecords();
          },
          error:function()
          {
            alert("Error");
          }
      });
  }
  else
  {
	 alert('Please fill all the field !');
  }
}
function deleteUserDetails(deleteid)
{
  var conf = confirm("Are you sure");
  if(conf==true)
  {
    $.ajax({
            url:"connect.php",
            type:'post',
            data: { deleteid:deleteid },
            success:function(data)
            {
              console.log("Record deleted successfully");
              readRecords();
            }, 
        });
  } 
}