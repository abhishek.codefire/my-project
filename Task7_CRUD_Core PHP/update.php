<html>
<head>
<title>Edit Data page</title>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<?php
include 'config.php';
$firstname = $lastname = $profile_pic = "";
$result = mysqli_query($conn,"SELECT first_name,last_name FROM users");
$row= mysqli_fetch_array($result);

if(isset($_POST['submit1']))
{	
    $id = $_GET['id']; 
	 $firstname = $_POST['firstname'];
	 $lastname = $_POST['lastname'];
	 $profile_pic = $_POST['profile_pic'];
     $sql = "UPDATE users SET  first_name='$firstname', last_name='$lastname', profile_pic='$profile_pic' where id='$id' ";
    // $sql = "UPDATE users SET  first_name='suraj' where id=6";
     if ($conn->query($sql) === TRUE) 
     {
       echo "Record updated successfully";
     } 
     else 
     {
       echo "Error updating record: " . $conn->error;
     }
     
     $conn->close();
}
?>

<div class="container">
<form style="width:400px; background-color:pink" method="POST">
<div class="card bg-secondary text-white text-center">
<div class="card-body"><h2>Update operation </h2></div>
</div><br>

<div class="form-group">
<label for="firstname">First Name :<span class="form-required"style="color:red"> *</span></label>
<input type="text" class="form-control"  name="firstname" value="<?php echo $row['first_name']; ?>" placeholder="Enter first name"><span id="username" class="text-danger  text-weight-bold" ></span>
</div>
<div class="form-group">
<label for="lastname"> Last Name :<span class="form-required"style="color:red"> *</span></label>
<input type="text" class="form-control"  name="lastname" value="<?php echo $row['last_name'];?>" placeholder="Enter last name"><span id="lastnames" class="text-danger  text-weight-bold" ></span>
</div>
<div class="form-group">
   <label >upload your pic <span class="form-required"style="color:red"> *</span></label>
   <input type="file" class="form-control-file" name="profile_pic"><span id="mypic" class="text-danger  text-weight-bold" ></span>
   
</div><a class="btn btn-primary" href="index.php" role="button">Back</a>
<button type="submit" class="btn btn-success"  name="submit1" value="Submit">Edit</button>
</form>
</div>
</body>
</html>