<?php include 'config.php';
$limit = 2;  
if (isset($_GET["page"]))
{
	$page  = $_GET["page"]; 
} 
else
{ 
    $page=1;
};  
$start_from = ($page-1) * $limit;  
$result = mysqli_query($conn,"SELECT * FROM users ORDER BY id ASC LIMIT $start_from, $limit");?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>CRUD</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <style>
      h1
      {
        text-align:center;
        background-color:orange;
        color:blue;
      }

    table,th,td{
        text-align:center;
    }
    </style>
</head>
<body>
<?php
if (mysqli_num_rows($result) > 0) {
?>
<div class="container"><br>
  <h1>PHP CRUD Application Design By Abhishek</h1><br><br><br>
  <div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Search">
      <button class="btn btn-secondary" type="submit">Search</button>
     </div>
  <button style="float:right; margin-bottom:20px" type="button" class="btn btn-success"><a href="add.php"style="color:white">Add User</a></button>
  <table class="table table-bordered table table-hover  table-striped">
    <thead class="table table-dark">
      <tr>
        <th>id</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Email</th>
        <th>View</th>
        <th>Edit</th> 
        <th>Delete</th>
      </tr>
    </thead>
    <?php
        while($row = mysqli_fetch_array($result)) 
        {
	?>
    <tbody>
      <tr>
        <td><?php echo $row["id"]; ?></td>
        <td><?php echo $row["first_name"]; ?></td>
        <td><?php echo $row["last_name"]; ?></td>
        <td><?php echo $row["email"]; ?></td>
        <td><button type="button" class="btn btn-info"><a style="color:white" href="view.php?id=<?php echo $row["id"];?>">View</a></button></td>
        <td><button type="button" class="btn btn-info"><a  style="color:white" href="update.php?id=<?php echo $row["id"];?>">Edit</a></button></td>
       <td><button type="button"  class="btn btn-danger"><a style="color:white" href="delete.php?id=<?php echo $row["id"];?>">Delete</a></button></td>
      </tr>
    </tbody>
    <?php
	    };
	?>
  </table>
 
<?php  
$result = mysqli_query($conn,"SELECT COUNT(id) FROM users"); 
$row = mysqli_fetch_row($result);  
$total_records = $row[0];  
$total_pages = ceil($total_records / $limit); 
/* echo  $total_pages; */
$pagLink = "<ul class='pagination justify-content-center' style='margin-right:50px'>";  
for ($i=1; $i<=$total_pages; $i++)
{
    $pagLink .= "<li class='page-item'><a class='page-link' href='index.php?page=".$i."'>".$i."</a></li>";	
}
echo $pagLink .= "</ul>";  
?>
<?php
}
else
{
    echo "No result found";
}
?>
</body>
</html>
