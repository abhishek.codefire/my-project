<!DOCTYPE html>
<html lang="en">
<head>
  <title>View Data page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 
<div class="container"><br>
<?php include 'config.php';?>
<?php
  $id = $_GET['id']; 
  $sql= "SELECT profile_pic,first_name,last_name,email FROM users WHERE  id='$id' ";
  $result = $conn->query($sql);
  if (mysqli_num_rows($result) > 0) 
  {
    while($row = mysqli_fetch_array($result)) 
    {
        $profile_pic= $row["profile_pic"];
        $firstname= $row["first_name"];
        $lastname= $row["last_name"];
        $email= $row["email"];
        ?>
        <div>
            <img src="admin/myimages/<?php echo $profile_pic; ?>" alt="image" width="200px" height="200px">
        </div>  
        <?php
        echo $firstname." " . $lastname . "<br>";
        echo $email;
    }
}
$conn->close();
?>
<br>
<a class="btn btn-primary" href="index.php" role="button">Back</a>
</body>
</html>

 